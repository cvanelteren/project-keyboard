#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 25 21:21:31 2018

@author: casper
"""

from shapely import geometry
from matplotlib.pyplot import subplots, close, cm, pause
from numpy import *
close('all')
from xml.dom import minidom

doc = minidom.parse('iris.svg')

xmlPoints = doc.getElementsByTagName('polygon')
polies = []

# extract polygons
for p in xmlPoints:
    p = p.getAttribute('points').split(' ')
    p = array( [ tuple(float(j) for j in i.split(','))for i in p ] )
    if len(p) > 4: # filter the edges
        polies.append(geometry.Polygon(p).centroid.xy)
polies = array(polies).squeeze()
fig, ax = subplots()
ax.scatter(*polies.T)
# remove left half
idx = where(polies[:, 0] <  160)[0]

# keep only right half and remove singleton dim
polies = polies[idx].squeeze()

# bin the data to extract the columns
dx    = 17 #ignore the special hooks; trial and error
bins  = arange(1, 7) * dx
idx   = digitize(polies[:, 0], bins, right = False)
uidx  = set(idx)
s     = zeros(polies.shape)
start = stop = 0

colors = linspace(0, 1, len(bins) + 1)
colors = cm.RdGy(colors)
fig, ax = subplots()
indexc = 0
for index, i in enumerate(uidx):
    jdx = where(idx == i)[0]
    tmp = polies[jdx]
    if tmp.shape[0] > 1:
        ii  = argsort(tmp[:, 1])
        tmp = tmp[ii]
#    if len(tmp) > 1:
#        tmp = sort(tmp, 0)
    stop += len(tmp)
    print(start, stop, tmp.shape)
    s[start : stop] = tmp
    start = stop
    for tt in tmp:
        ax.scatter(*tt, color = colors[index], edgecolor = 'k', label = indexc)
        indexc += 1
#        pause(.2)

#ax.scatter(*polies.T, alpha = .5, color = 'Y')
with open('centroids.txt', 'w') as f:
    for center in s:
        f.write('\t'.join(str(float(i)) for i in center) + '\n')
