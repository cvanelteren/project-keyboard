EESchema Schematic File Version 4
LIBS:fifth-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ -5050 4900
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K1
U 1 1 5A808C37
P 5750 1050
F 0 "K1" H 5700 1050 60  0000 C CNN
F 1 "KEYSW" H 5750 950 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 5750 1050 60  0001 C CNN
F 3 "" H 5750 1050 60  0000 C CNN
	1    5750 1050
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D1
U 1 1 5A808D18
P 5450 1300
F 0 "D1" H 5450 1400 50  0000 C CNN
F 1 "D" H 5450 1200 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 5450 1300 50  0001 C CNN
F 3 "" H 5450 1300 50  0001 C CNN
	1    5450 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5450 1050 5450 1150
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K6
U 1 1 5A809089
P 6550 1050
F 0 "K6" H 6500 1050 60  0000 C CNN
F 1 "KEYSW" H 6550 950 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 6550 1050 60  0001 C CNN
F 3 "" H 6550 1050 60  0000 C CNN
	1    6550 1050
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D6
U 1 1 5A80908F
P 6250 1300
F 0 "D6" H 6250 1400 50  0000 C CNN
F 1 "D" H 6250 1200 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 6250 1300 50  0001 C CNN
F 3 "" H 6250 1300 50  0001 C CNN
	1    6250 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 1050 6250 1150
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K11
U 1 1 5A8091F6
P 7350 1050
F 0 "K11" H 7300 1050 60  0000 C CNN
F 1 "KEYSW" H 7350 950 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 7350 1050 60  0001 C CNN
F 3 "" H 7350 1050 60  0000 C CNN
	1    7350 1050
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D11
U 1 1 5A8091FC
P 7050 1300
F 0 "D11" H 7050 1400 50  0000 C CNN
F 1 "D" H 7050 1200 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 7050 1300 50  0001 C CNN
F 3 "" H 7050 1300 50  0001 C CNN
	1    7050 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7050 1050 7050 1150
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K16
U 1 1 5A809203
P 8150 1050
F 0 "K16" H 8100 1050 60  0000 C CNN
F 1 "KEYSW" H 8150 950 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 8150 1050 60  0001 C CNN
F 3 "" H 8150 1050 60  0000 C CNN
	1    8150 1050
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D16
U 1 1 5A809209
P 7850 1300
F 0 "D16" H 7850 1400 50  0000 C CNN
F 1 "D" H 7850 1200 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 7850 1300 50  0001 C CNN
F 3 "" H 7850 1300 50  0001 C CNN
	1    7850 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7850 1050 7850 1150
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K21
U 1 1 5A80948D
P 8950 1050
F 0 "K21" H 8900 1050 60  0000 C CNN
F 1 "KEYSW" H 8950 950 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 8950 1050 60  0001 C CNN
F 3 "" H 8950 1050 60  0000 C CNN
	1    8950 1050
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D21
U 1 1 5A809493
P 8650 1300
F 0 "D21" H 8650 1400 50  0000 C CNN
F 1 "D" H 8650 1200 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 8650 1300 50  0001 C CNN
F 3 "" H 8650 1300 50  0001 C CNN
	1    8650 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8650 1050 8650 1150
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K26
U 1 1 5A80949A
P 9750 1050
F 0 "K26" H 9700 1050 60  0000 C CNN
F 1 "KEYSW" H 9750 950 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 9750 1050 60  0001 C CNN
F 3 "" H 9750 1050 60  0000 C CNN
	1    9750 1050
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D26
U 1 1 5A8094A0
P 9450 1300
F 0 "D26" H 9450 1400 50  0000 C CNN
F 1 "D" H 9450 1200 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 9450 1300 50  0001 C CNN
F 3 "" H 9450 1300 50  0001 C CNN
	1    9450 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9450 1050 9450 1150
Wire Wire Line
	5350 1450 5450 1450
Connection ~ 6250 1450
Connection ~ 7050 1450
Connection ~ 7850 1450
Connection ~ 8650 1450
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K2
U 1 1 5A809C1D
P 5750 1800
F 0 "K2" H 5700 1800 60  0000 C CNN
F 1 "KEYSW" H 5750 1700 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 5750 1800 60  0001 C CNN
F 3 "" H 5750 1800 60  0000 C CNN
	1    5750 1800
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D2
U 1 1 5A809C23
P 5450 2050
F 0 "D2" H 5450 2150 50  0000 C CNN
F 1 "D" H 5450 1950 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 5450 2050 50  0001 C CNN
F 3 "" H 5450 2050 50  0001 C CNN
	1    5450 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5450 1800 5450 1900
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K7
U 1 1 5A809C2A
P 6550 1800
F 0 "K7" H 6500 1800 60  0000 C CNN
F 1 "KEYSW" H 6550 1700 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 6550 1800 60  0001 C CNN
F 3 "" H 6550 1800 60  0000 C CNN
	1    6550 1800
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D7
U 1 1 5A809C30
P 6250 2050
F 0 "D7" H 6250 2150 50  0000 C CNN
F 1 "D" H 6250 1950 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 6250 2050 50  0001 C CNN
F 3 "" H 6250 2050 50  0001 C CNN
	1    6250 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 1800 6250 1900
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K12
U 1 1 5A809C37
P 7350 1800
F 0 "K12" H 7300 1800 60  0000 C CNN
F 1 "KEYSW" H 7350 1700 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 7350 1800 60  0001 C CNN
F 3 "" H 7350 1800 60  0000 C CNN
	1    7350 1800
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D12
U 1 1 5A809C3D
P 7050 2050
F 0 "D12" H 7050 2150 50  0000 C CNN
F 1 "D" H 7050 1950 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 7050 2050 50  0001 C CNN
F 3 "" H 7050 2050 50  0001 C CNN
	1    7050 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7050 1800 7050 1900
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K17
U 1 1 5A809C44
P 8150 1800
F 0 "K17" H 8100 1800 60  0000 C CNN
F 1 "KEYSW" H 8150 1700 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 8150 1800 60  0001 C CNN
F 3 "" H 8150 1800 60  0000 C CNN
	1    8150 1800
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D17
U 1 1 5A809C4A
P 7850 2050
F 0 "D17" H 7850 2150 50  0000 C CNN
F 1 "D" H 7850 1950 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 7850 2050 50  0001 C CNN
F 3 "" H 7850 2050 50  0001 C CNN
	1    7850 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7850 1800 7850 1900
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K22
U 1 1 5A809C51
P 8950 1800
F 0 "K22" H 8900 1800 60  0000 C CNN
F 1 "KEYSW" H 8950 1700 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 8950 1800 60  0001 C CNN
F 3 "" H 8950 1800 60  0000 C CNN
	1    8950 1800
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D22
U 1 1 5A809C57
P 8650 2050
F 0 "D22" H 8650 2150 50  0000 C CNN
F 1 "D" H 8650 1950 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 8650 2050 50  0001 C CNN
F 3 "" H 8650 2050 50  0001 C CNN
	1    8650 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8650 1800 8650 1900
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K27
U 1 1 5A809C5E
P 9750 1800
F 0 "K27" H 9700 1800 60  0000 C CNN
F 1 "KEYSW" H 9750 1700 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 9750 1800 60  0001 C CNN
F 3 "" H 9750 1800 60  0000 C CNN
	1    9750 1800
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D27
U 1 1 5A809C64
P 9450 2050
F 0 "D27" H 9450 2150 50  0000 C CNN
F 1 "D" H 9450 1950 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 9450 2050 50  0001 C CNN
F 3 "" H 9450 2050 50  0001 C CNN
	1    9450 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9450 1800 9450 1900
Wire Wire Line
	5350 2200 5450 2200
Connection ~ 6250 2200
Connection ~ 7050 2200
Connection ~ 7850 2200
Connection ~ 8650 2200
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K3
U 1 1 5A80AB8A
P 5750 2500
F 0 "K3" H 5700 2500 60  0000 C CNN
F 1 "KEYSW" H 5750 2400 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 5750 2500 60  0001 C CNN
F 3 "" H 5750 2500 60  0000 C CNN
	1    5750 2500
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D3
U 1 1 5A80AB90
P 5450 2750
F 0 "D3" H 5450 2850 50  0000 C CNN
F 1 "D" H 5450 2650 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 5450 2750 50  0001 C CNN
F 3 "" H 5450 2750 50  0001 C CNN
	1    5450 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5450 2500 5450 2600
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K8
U 1 1 5A80AB97
P 6550 2500
F 0 "K8" H 6500 2500 60  0000 C CNN
F 1 "KEYSW" H 6550 2400 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 6550 2500 60  0001 C CNN
F 3 "" H 6550 2500 60  0000 C CNN
	1    6550 2500
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D8
U 1 1 5A80AB9D
P 6250 2750
F 0 "D8" H 6250 2850 50  0000 C CNN
F 1 "D" H 6250 2650 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 6250 2750 50  0001 C CNN
F 3 "" H 6250 2750 50  0001 C CNN
	1    6250 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 2500 6250 2600
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K13
U 1 1 5A80ABA4
P 7350 2500
F 0 "K13" H 7300 2500 60  0000 C CNN
F 1 "KEYSW" H 7350 2400 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 7350 2500 60  0001 C CNN
F 3 "" H 7350 2500 60  0000 C CNN
	1    7350 2500
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D13
U 1 1 5A80ABAA
P 7050 2750
F 0 "D13" H 7050 2850 50  0000 C CNN
F 1 "D" H 7050 2650 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 7050 2750 50  0001 C CNN
F 3 "" H 7050 2750 50  0001 C CNN
	1    7050 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7050 2500 7050 2600
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K18
U 1 1 5A80ABB1
P 8150 2500
F 0 "K18" H 8100 2500 60  0000 C CNN
F 1 "KEYSW" H 8150 2400 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 8150 2500 60  0001 C CNN
F 3 "" H 8150 2500 60  0000 C CNN
	1    8150 2500
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D18
U 1 1 5A80ABB7
P 7850 2750
F 0 "D18" H 7850 2850 50  0000 C CNN
F 1 "D" H 7850 2650 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 7850 2750 50  0001 C CNN
F 3 "" H 7850 2750 50  0001 C CNN
	1    7850 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7850 2500 7850 2600
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K23
U 1 1 5A80ABBE
P 8950 2500
F 0 "K23" H 8900 2500 60  0000 C CNN
F 1 "KEYSW" H 8950 2400 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 8950 2500 60  0001 C CNN
F 3 "" H 8950 2500 60  0000 C CNN
	1    8950 2500
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D23
U 1 1 5A80ABC4
P 8650 2750
F 0 "D23" H 8650 2850 50  0000 C CNN
F 1 "D" H 8650 2650 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 8650 2750 50  0001 C CNN
F 3 "" H 8650 2750 50  0001 C CNN
	1    8650 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8650 2500 8650 2600
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K28
U 1 1 5A80ABCB
P 9750 2500
F 0 "K28" H 9700 2500 60  0000 C CNN
F 1 "KEYSW" H 9750 2400 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 9750 2500 60  0001 C CNN
F 3 "" H 9750 2500 60  0000 C CNN
	1    9750 2500
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D28
U 1 1 5A80ABD1
P 9450 2750
F 0 "D28" H 9450 2850 50  0000 C CNN
F 1 "D" H 9450 2650 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 9450 2750 50  0001 C CNN
F 3 "" H 9450 2750 50  0001 C CNN
	1    9450 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9450 2500 9450 2600
Wire Wire Line
	5350 2900 5450 2900
Connection ~ 6250 2900
Connection ~ 7050 2900
Connection ~ 7850 2900
Connection ~ 8650 2900
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K4
U 1 1 5A80ABEB
P 5750 3250
F 0 "K4" H 5700 3250 60  0000 C CNN
F 1 "KEYSW" H 5750 3150 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 5750 3250 60  0001 C CNN
F 3 "" H 5750 3250 60  0000 C CNN
	1    5750 3250
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D4
U 1 1 5A80ABF1
P 5450 3500
F 0 "D4" H 5450 3600 50  0000 C CNN
F 1 "D" H 5450 3400 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 5450 3500 50  0001 C CNN
F 3 "" H 5450 3500 50  0001 C CNN
	1    5450 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5450 3250 5450 3350
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K9
U 1 1 5A80ABF8
P 6550 3250
F 0 "K9" H 6500 3250 60  0000 C CNN
F 1 "KEYSW" H 6550 3150 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 6550 3250 60  0001 C CNN
F 3 "" H 6550 3250 60  0000 C CNN
	1    6550 3250
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D9
U 1 1 5A80ABFE
P 6250 3500
F 0 "D9" H 6250 3600 50  0000 C CNN
F 1 "D" H 6250 3400 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 6250 3500 50  0001 C CNN
F 3 "" H 6250 3500 50  0001 C CNN
	1    6250 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 3250 6250 3350
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K14
U 1 1 5A80AC05
P 7350 3250
F 0 "K14" H 7300 3250 60  0000 C CNN
F 1 "KEYSW" H 7350 3150 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 7350 3250 60  0001 C CNN
F 3 "" H 7350 3250 60  0000 C CNN
	1    7350 3250
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D14
U 1 1 5A80AC0B
P 7050 3500
F 0 "D14" H 7050 3600 50  0000 C CNN
F 1 "D" H 7050 3400 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 7050 3500 50  0001 C CNN
F 3 "" H 7050 3500 50  0001 C CNN
	1    7050 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7050 3250 7050 3350
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K19
U 1 1 5A80AC12
P 8150 3250
F 0 "K19" H 8100 3250 60  0000 C CNN
F 1 "KEYSW" H 8150 3150 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 8150 3250 60  0001 C CNN
F 3 "" H 8150 3250 60  0000 C CNN
	1    8150 3250
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D19
U 1 1 5A80AC18
P 7850 3500
F 0 "D19" H 7850 3600 50  0000 C CNN
F 1 "D" H 7850 3400 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 7850 3500 50  0001 C CNN
F 3 "" H 7850 3500 50  0001 C CNN
	1    7850 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7850 3250 7850 3350
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K24
U 1 1 5A80AC1F
P 8950 3250
F 0 "K24" H 8900 3250 60  0000 C CNN
F 1 "KEYSW" H 8950 3150 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 8950 3250 60  0001 C CNN
F 3 "" H 8950 3250 60  0000 C CNN
	1    8950 3250
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D24
U 1 1 5A80AC25
P 8650 3500
F 0 "D24" H 8650 3600 50  0000 C CNN
F 1 "D" H 8650 3400 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 8650 3500 50  0001 C CNN
F 3 "" H 8650 3500 50  0001 C CNN
	1    8650 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8650 3250 8650 3350
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K29
U 1 1 5A80AC2C
P 9750 3250
F 0 "K29" H 9700 3250 60  0000 C CNN
F 1 "KEYSW" H 9750 3150 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 9750 3250 60  0001 C CNN
F 3 "" H 9750 3250 60  0000 C CNN
	1    9750 3250
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D29
U 1 1 5A80AC32
P 9450 3500
F 0 "D29" H 9450 3600 50  0000 C CNN
F 1 "D" H 9450 3400 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 9450 3500 50  0001 C CNN
F 3 "" H 9450 3500 50  0001 C CNN
	1    9450 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9450 3250 9450 3350
Wire Wire Line
	5350 3650 5450 3650
Connection ~ 6250 3650
Connection ~ 7050 3650
Connection ~ 7850 3650
Connection ~ 8650 3650
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K20
U 1 1 5A80E493
P 8150 4000
F 0 "K20" H 8100 4000 60  0000 C CNN
F 1 "KEYSW" H 8150 3900 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 8150 4000 60  0001 C CNN
F 3 "" H 8150 4000 60  0000 C CNN
	1    8150 4000
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D20
U 1 1 5A80E499
P 7850 4250
F 0 "D20" H 7850 4350 50  0000 C CNN
F 1 "D" H 7850 4150 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 7850 4250 50  0001 C CNN
F 3 "" H 7850 4250 50  0001 C CNN
	1    7850 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7850 4000 7850 4100
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K25
U 1 1 5A80E4A0
P 8950 4000
F 0 "K25" H 8900 4000 60  0000 C CNN
F 1 "KEYSW" H 8950 3900 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap" H 8950 4000 60  0001 C CNN
F 3 "" H 8950 4000 60  0000 C CNN
	1    8950 4000
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D25
U 1 1 5A80E4A6
P 8650 4250
F 0 "D25" H 8650 4350 50  0000 C CNN
F 1 "D" H 8650 4150 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 8650 4250 50  0001 C CNN
F 3 "" H 8650 4250 50  0001 C CNN
	1    8650 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8650 4000 8650 4100
$Comp
L fifth-rescue:KEYSW-redox_rev1-rescue K30
U 1 1 5A80E4AD
P 9750 4000
F 0 "K30" H 9700 4000 60  0000 C CNN
F 1 "KEYSW" H 9750 3900 60  0001 C CNN
F 2 "kbd:CherryMX_Hotswap_1.5u" H 9750 4000 60  0001 C CNN
F 3 "" H 9750 4000 60  0000 C CNN
	1    9750 4000
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:D-redox_rev1-rescue D30
U 1 1 5A80E4B3
P 9450 4250
F 0 "D30" H 9450 4350 50  0000 C CNN
F 1 "D" H 9450 4150 50  0000 C CNN
F 2 "kbd:D3_TH_SMD" H 9450 4250 50  0001 C CNN
F 3 "" H 9450 4250 50  0001 C CNN
	1    9450 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9450 4000 9450 4100
Wire Wire Line
	7750 4400 7850 4400
Connection ~ 8650 4400
Wire Wire Line
	6050 850  6050 1050
Connection ~ 6050 1050
Connection ~ 6050 1800
Connection ~ 6050 2500
Wire Wire Line
	6850 850  6850 1050
Connection ~ 6850 1050
Connection ~ 6850 1800
Connection ~ 6850 2500
Connection ~ 5450 1450
Connection ~ 5450 2200
Connection ~ 5450 2900
Connection ~ 5450 3650
Wire Wire Line
	7650 850  7650 1050
Connection ~ 7650 1050
Connection ~ 7650 1800
Connection ~ 7650 2500
Wire Wire Line
	8450 850  8450 1050
Connection ~ 8450 1050
Connection ~ 8450 1800
Connection ~ 8450 2500
Wire Wire Line
	9250 850  9250 1050
Connection ~ 9250 1050
Connection ~ 9250 1800
Connection ~ 9250 2500
Wire Wire Line
	10050 900  10050 1050
Connection ~ 10050 1050
Connection ~ 10050 1800
Text GLabel 5350 1450 0    60   Input ~ 0
row0
Text GLabel 5350 2200 0    60   Input ~ 0
row1
Text GLabel 5350 2900 0    60   Input ~ 0
row2
Text GLabel 5350 3650 0    60   Input ~ 0
row3
Text GLabel 7750 4400 0    60   Input ~ 0
row4
Text GLabel 6050 850  1    60   Input ~ 0
col0
Text GLabel 6850 850  1    60   Input ~ 0
col1
Text GLabel 7650 850  1    60   Input ~ 0
col2
Text GLabel 8450 850  1    60   Input ~ 0
col3
Text GLabel 9250 850  1    60   Input ~ 0
col4
Text GLabel 10050 900  1    60   Input ~ 0
col5
Wire Wire Line
	6250 1450 7050 1450
Wire Wire Line
	7050 1450 7850 1450
Wire Wire Line
	7850 1450 8650 1450
Wire Wire Line
	8650 1450 9450 1450
Wire Wire Line
	6250 2200 7050 2200
Wire Wire Line
	7050 2200 7850 2200
Wire Wire Line
	7850 2200 8650 2200
Wire Wire Line
	8650 2200 9450 2200
Wire Wire Line
	6250 2900 7050 2900
Wire Wire Line
	7050 2900 7850 2900
Wire Wire Line
	7850 2900 8650 2900
Wire Wire Line
	8650 2900 9450 2900
Wire Wire Line
	6250 3650 7050 3650
Wire Wire Line
	7050 3650 7850 3650
Wire Wire Line
	7850 3650 8650 3650
Wire Wire Line
	8650 3650 9450 3650
Wire Wire Line
	8650 4400 9450 4400
Wire Wire Line
	6050 1050 6050 1800
Wire Wire Line
	6050 1800 6050 2500
Wire Wire Line
	6050 2500 6050 3250
Wire Wire Line
	8450 3250 8450 4000
Wire Wire Line
	6850 1050 6850 1800
Wire Wire Line
	6850 1800 6850 2500
Wire Wire Line
	6850 2500 6850 3250
Wire Wire Line
	9250 3250 9250 4000
Wire Wire Line
	5450 1450 6250 1450
Wire Wire Line
	5450 2200 6250 2200
Wire Wire Line
	5450 2900 6250 2900
Wire Wire Line
	5450 3650 6250 3650
Wire Wire Line
	7850 4400 8650 4400
Wire Wire Line
	7650 1050 7650 1800
Wire Wire Line
	7650 1800 7650 2500
Wire Wire Line
	7650 2500 7650 3250
Wire Wire Line
	10050 3250 10050 4000
Wire Wire Line
	8450 1050 8450 1800
Wire Wire Line
	8450 1800 8450 2500
Wire Wire Line
	8450 2500 8450 3250
Wire Wire Line
	9250 1050 9250 1800
Wire Wire Line
	9250 1800 9250 2500
Wire Wire Line
	9250 2500 9250 3250
Wire Wire Line
	10050 1050 10050 1800
$Comp
L fifth-rescue:Core51822B-core51822b U_LEFT1
U 1 1 5B82A47C
P 1450 1250
F 0 "U_LEFT1" H 1450 2137 60  0000 C CNN
F 1 "Core51822B" H 1450 2031 60  0000 C CNN
F 2 "redox_w_footprints:MY-YJ-14015-Module" H 1450 1350 60  0001 C CNN
F 3 "" H 1450 1350 60  0001 C CNN
	1    1450 1250
	1    0    0    -1  
$EndComp
Text GLabel 2400 850  2    60   Input ~ 0
row0
Text GLabel 2400 950  2    60   Input ~ 0
row1
Text GLabel 2400 1050 2    60   Input ~ 0
row2
Text GLabel 2400 1550 2    60   Input ~ 0
row3
Text GLabel 2400 1650 2    60   Input ~ 0
row4
$Comp
L fifth-rescue:GND-power #PWR0101
U 1 1 5B82E11B
P 500 750
F 0 "#PWR0101" H 500 500 50  0001 C CNN
F 1 "GND" V 505 622 50  0000 R CNN
F 2 "" H 500 750 50  0001 C CNN
F 3 "" H 500 750 50  0001 C CNN
	1    500  750 
	0    1    1    0   
$EndComp
$Comp
L fifth-rescue:GND-power #PWR0102
U 1 1 5B82E2DD
P 500 1750
F 0 "#PWR0102" H 500 1500 50  0001 C CNN
F 1 "GND" V 505 1622 50  0000 R CNN
F 2 "" H 500 1750 50  0001 C CNN
F 3 "" H 500 1750 50  0001 C CNN
	1    500  1750
	0    1    1    0   
$EndComp
$Comp
L fifth-rescue:GND-power #PWR0103
U 1 1 5B82E5B8
P -2900 650
F 0 "#PWR0103" H -2900 400 50  0001 C CNN
F 1 "GND" V -2895 522 50  0000 R CNN
F 2 "" H -2900 650 50  0001 C CNN
F 3 "" H -2900 650 50  0001 C CNN
	1    -2900 650 
	0    1    1    0   
$EndComp
$Comp
L fifth-rescue:GND-power #PWR0104
U 1 1 5B82E69B
P -2900 1650
F 0 "#PWR0104" H -2900 1400 50  0001 C CNN
F 1 "GND" V -2895 1522 50  0000 R CNN
F 2 "" H -2900 1650 50  0001 C CNN
F 3 "" H -2900 1650 50  0001 C CNN
	1    -2900 1650
	0    1    1    0   
$EndComp
$Comp
L fifth-rescue:VCC-power #PWR0105
U 1 1 5B82E935
P -2900 1750
F 0 "#PWR0105" H -2900 1600 50  0001 C CNN
F 1 "VCC" V -2882 1877 50  0000 L CNN
F 2 "" H -2900 1750 50  0001 C CNN
F 3 "" H -2900 1750 50  0001 C CNN
	1    -2900 1750
	0    -1   -1   0   
$EndComp
$Comp
L fifth-rescue:VCC-power #PWR0106
U 1 1 5B82EAF7
P 500 1850
F 0 "#PWR0106" H 500 1700 50  0001 C CNN
F 1 "VCC" V 518 1977 50  0000 L CNN
F 2 "" H 500 1850 50  0001 C CNN
F 3 "" H 500 1850 50  0001 C CNN
	1    500  1850
	0    -1   -1   0   
$EndComp
$Comp
L fifth-rescue:Conn_01x04_Male-Connector J_PROG_PORT1
U 1 1 5B82F178
P 1700 6000
F 0 "J_PROG_PORT1" H 1806 6278 50  0000 C CNN
F 1 "Conn_01x04_Male" H 1806 6187 50  0000 C CNN
F 2 "redox_w_footprints:PinHeader_1x04_P2.54mm_Horizontal" H 1700 6000 50  0001 C CNN
F 3 "~" H 1700 6000 50  0001 C CNN
	1    1700 6000
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:VCC-power #PWR0107
U 1 1 5B82F8D5
P 1900 5900
F 0 "#PWR0107" H 1900 5750 50  0001 C CNN
F 1 "VCC" V 1917 6028 50  0000 L CNN
F 2 "" H 1900 5900 50  0001 C CNN
F 3 "" H 1900 5900 50  0001 C CNN
	1    1900 5900
	0    1    1    0   
$EndComp
$Comp
L fifth-rescue:GND-power #PWR0108
U 1 1 5B82FA96
P 1900 6200
F 0 "#PWR0108" H 1900 5950 50  0001 C CNN
F 1 "GND" V 1905 6072 50  0000 R CNN
F 2 "" H 1900 6200 50  0001 C CNN
F 3 "" H 1900 6200 50  0001 C CNN
	1    1900 6200
	0    -1   -1   0   
$EndComp
Text GLabel -1000 1050 2    60   Input ~ 0
SWCLK
Text GLabel -1000 1150 2    60   Input ~ 0
SWDIO
Text GLabel 1900 6000 2    60   Input ~ 0
SWCLK
Text GLabel 1900 6100 2    60   Input ~ 0
SWDIO
Text GLabel 2400 1150 2    60   Input ~ 0
SWCLK
Text GLabel 2400 1250 2    60   Input ~ 0
SWDIO
NoConn ~ 500  1150
NoConn ~ 500  1250
NoConn ~ 500  1350
NoConn ~ 500  1450
NoConn ~ 2400 750 
NoConn ~ 2400 1350
NoConn ~ 2400 1450
NoConn ~ 2400 1750
NoConn ~ 2400 1850
NoConn ~ -2900 1050
NoConn ~ -2900 1150
NoConn ~ -2900 1250
NoConn ~ -2900 1350
NoConn ~ -1000 650 
NoConn ~ -1000 1250
NoConn ~ -1000 1350
NoConn ~ -1000 1650
NoConn ~ -1000 1750
Text GLabel 900  2300 3    60   Input ~ 0
col0
Text GLabel 1000 2300 3    60   Input ~ 0
col1
Text GLabel 1200 2300 3    60   Input ~ 0
col2
Text GLabel 1300 2300 3    60   Input ~ 0
col3
Text GLabel 1400 2300 3    60   Input ~ 0
col4
Text GLabel 1500 2300 3    60   Input ~ 0
col5
NoConn ~ 1100 2300
NoConn ~ 1700 2300
NoConn ~ 1800 2300
NoConn ~ 1900 2300
NoConn ~ 2000 2300
NoConn ~ -2500 2200
NoConn ~ -2400 2200
NoConn ~ -2300 2200
NoConn ~ -2200 2200
NoConn ~ -1600 2200
$Comp
L fifth-rescue:Core51822B-core51822b U_RIGHT1
U 1 1 5B82A707
P -1950 1150
F 0 "U_RIGHT1" H -1950 2037 60  0000 C CNN
F 1 "Core51822B" H -1950 1931 60  0000 C CNN
F 2 "redox_w_footprints:MY-YJ-14015-Module" H -1950 1250 60  0001 C CNN
F 3 "" H -1950 1250 60  0001 C CNN
	1    -1950 1150
	1    0    0    -1  
$EndComp
Text GLabel -2900 750  0    60   Input ~ 0
row0
Text GLabel -2900 850  0    60   Input ~ 0
row1
Text GLabel -2900 950  0    60   Input ~ 0
row2
Text GLabel -2900 1450 0    60   Input ~ 0
row3
Text GLabel -2900 1550 0    60   Input ~ 0
row4
Text GLabel -1400 2200 3    60   Input ~ 0
col0
Text GLabel -1500 2200 3    60   Input ~ 0
col1
Text GLabel -1700 2200 3    60   Input ~ 0
col2
Text GLabel -1800 2200 3    60   Input ~ 0
col3
Text GLabel -1900 2200 3    60   Input ~ 0
col4
Text GLabel -2000 2200 3    60   Input ~ 0
col5
Connection ~ 7850 4400
Text GLabel 3050 5850 0    50   Input ~ 0
5V
Wire Wire Line
	4250 6450 4250 5850
Text GLabel 6300 5800 2    50   Input ~ 0
VBAT
Wire Wire Line
	5350 5800 5800 5800
$Comp
L Device:R R1
U 1 1 5C18FAA7
P 3700 7200
F 0 "R1" H 3770 7246 50  0000 L CNN
F 1 "2k" H 3770 7155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3630 7200 50  0001 C CNN
F 3 "~" H 3700 7200 50  0001 C CNN
	1    3700 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 6850 3700 6850
Wire Wire Line
	3700 6850 3700 7050
Wire Wire Line
	3350 7550 3700 7550
Wire Wire Line
	3700 7550 3700 7350
Wire Wire Line
	3350 7550 3350 6700
Wire Wire Line
	3050 5850 3350 5850
Connection ~ 3350 5850
$Comp
L Device:C C1
U 1 1 5C19E7B9
P 3350 6550
F 0 "C1" H 3465 6596 50  0000 L CNN
F 1 "4u7F" H 3465 6505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3388 6400 50  0001 C CNN
F 3 "~" H 3350 6550 50  0001 C CNN
	1    3350 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 6400 3350 5850
Wire Wire Line
	3350 5850 4250 5850
$Comp
L Device:R R2
U 1 1 5C1A41F9
P 5150 6150
F 0 "R2" H 5220 6196 50  0000 L CNN
F 1 "470 ohm" H 5220 6105 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5080 6150 50  0001 C CNN
F 3 "~" H 5150 6150 50  0001 C CNN
	1    5150 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D100
U 1 1 5C1B384D
P 5150 6450
F 0 "D100" V 5188 6332 50  0000 R CNN
F 1 "CHARGE" V 5097 6332 50  0000 R CNN
F 2 "Diode_SMD:D_0603_1608Metric" H 5150 6450 50  0001 C CNN
F 3 "~" H 5150 6450 50  0001 C CNN
	1    5150 6450
	0    -1   -1   0   
$EndComp
$Comp
L Battery_Management:MCP73831-2-OT U1_BAT1
U 1 1 5C1C3036
P 4250 6750
F 0 "U1_BAT1" H 4250 7228 50  0000 C CNN
F 1 "MCP73831-2-OT" H 4250 7137 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4300 6500 50  0001 L CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001984g.pdf" H 4100 6700 50  0001 C CNN
	1    4250 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6650 5350 6650
$Comp
L Device:R R3
U 1 1 5C1DEE4D
P 5150 7300
F 0 "R3" H 5220 7346 50  0000 L CNN
F 1 "470 ohm" H 5220 7255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5080 7300 50  0001 C CNN
F 3 "~" H 5150 7300 50  0001 C CNN
	1    5150 7300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D101
U 1 1 5C1DEE54
P 5150 7000
F 0 "D101" V 5188 6882 50  0000 R CNN
F 1 "READY" V 5097 6882 50  0000 R CNN
F 2 "Diode_SMD:D_0603_1608Metric" H 5150 7000 50  0001 C CNN
F 3 "~" H 5150 7000 50  0001 C CNN
	1    5150 7000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 5800 5350 6650
Wire Wire Line
	4650 6850 5150 6850
Wire Wire Line
	5150 6850 5150 6600
Connection ~ 5150 6850
Wire Wire Line
	3700 7550 4250 7550
Wire Wire Line
	4250 7550 4250 7050
Connection ~ 3700 7550
Wire Wire Line
	5150 7450 5150 7550
Wire Wire Line
	5150 7550 4250 7550
Connection ~ 4250 7550
$Comp
L Device:C C2
U 1 1 5C1F3E03
P 5800 6750
F 0 "C2" H 5915 6796 50  0000 L CNN
F 1 "4u7F" H 5915 6705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5838 6600 50  0001 C CNN
F 3 "~" H 5800 6750 50  0001 C CNN
	1    5800 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 7550 5800 7550
Wire Wire Line
	5800 7550 5800 6900
Connection ~ 5150 7550
Wire Wire Line
	5800 6600 5800 5800
Connection ~ 5800 5800
Wire Wire Line
	5800 5800 6300 5800
Wire Wire Line
	5150 6000 5150 5850
Wire Wire Line
	5150 5850 4250 5850
Connection ~ 4250 5850
$Comp
L Device:D_Schottky D0
U 1 1 5C207B53
P 8800 5400
F 0 "D0" V 8846 5321 50  0000 R CNN
F 1 "NSR0320" V 8755 5321 50  0000 R CNN
F 2 "Diode_SMD:D_SOD-323" H 8800 5400 50  0001 C CNN
F 3 "~" H 8800 5400 50  0001 C CNN
	1    8800 5400
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C3
U 1 1 5C207F9F
P 8800 5900
F 0 "C3" H 8915 5946 50  0000 L CNN
F 1 "1uF" H 8915 5855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8838 5750 50  0001 C CNN
F 3 "~" H 8800 5900 50  0001 C CNN
	1    8800 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5C212191
P 8350 5900
F 0 "R4" H 8420 5946 50  0000 L CNN
F 1 "100k" H 8420 5855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8280 5900 50  0001 C CNN
F 3 "~" H 8350 5900 50  0001 C CNN
	1    8350 5900
	1    0    0    -1  
$EndComp
Text GLabel 7350 5150 0    50   Input ~ 0
5V
Text GLabel 7400 5550 0    50   Input ~ 0
VBAT
Text GLabel 9500 5550 2    50   Input ~ 0
VCC
Connection ~ 8800 5550
Wire Wire Line
	8800 5550 9500 5550
Wire Wire Line
	7400 5550 7650 5550
Wire Wire Line
	8800 5550 8800 5750
Wire Wire Line
	8350 5750 8350 5150
Wire Wire Line
	8350 6050 8350 6550
Wire Wire Line
	8350 6550 8800 6550
Wire Wire Line
	8800 6550 8800 6050
Text GLabel 10950 5050 2    50   Input ~ 0
5V
NoConn ~ 11250 4450
NoConn ~ 10950 5450
NoConn ~ 10950 5350
Wire Wire Line
	8800 5250 8800 5150
Wire Wire Line
	8800 5150 8350 5150
Connection ~ 8350 5150
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 5C2E5756
P 1800 6700
F 0 "J1" H 1828 6676 50  0000 L CNN
F 1 "JST CONNECTOR" H 1828 6585 50  0000 L CNN
F 2 "Connector_JST:JST_SUR_SM02B-SURS-TF_1x02-1MP_P0.80mm_Horizontal" H 1800 6700 50  0001 C CNN
F 3 "~" H 1800 6700 50  0001 C CNN
	1    1800 6700
	1    0    0    -1  
$EndComp
Text GLabel 1350 6700 0    50   Input ~ 0
VBAT
Wire Wire Line
	1600 6800 1350 6800
Wire Wire Line
	1350 6700 1600 6700
$Comp
L Transistor_FET:Si2319CDS Q1
U 1 1 5C2F24D1
P 7850 5450
F 0 "Q1" V 8100 5450 50  0000 C CNN
F 1 "SI2301" V 8191 5450 50  0000 C CNN
F 2 "kicad-libraries:SOT23-3" H 8050 5375 50  0001 L CIN
F 3 "http://www.vishay.com/docs/66709/si2319cd.pdf" H 7850 5450 50  0001 L CNN
	1    7850 5450
	0    -1   1    0   
$EndComp
Wire Wire Line
	8050 5550 8800 5550
Wire Wire Line
	7350 5150 7850 5150
Wire Wire Line
	7850 5250 7850 5150
Connection ~ 7850 5150
Wire Wire Line
	7850 5150 8350 5150
$Comp
L fifth-rescue:Switch_SW_SPDT-redox_rev1-cache SW_POWER1
U 1 1 5C306455
P 1450 4950
F 0 "SW_POWER1" H 1450 5235 50  0000 C CNN
F 1 "Switch_SW_SPDT" H 1450 5144 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPDT_PCM12" H 1450 4950 50  0001 C CNN
F 3 "" H 1450 4950 50  0001 C CNN
	1    1450 4950
	1    0    0    -1  
$EndComp
Text GLabel 1250 4950 0    50   Input ~ 0
VBAT
Text GLabel 1650 4850 2    50   Input ~ 0
VBAT
Text GLabel 2050 2800 0    50   Input ~ 0
VCC
Text GLabel 4400 2800 2    50   Input ~ 0
VCC
Wire Wire Line
	2550 2800 2200 2800
$Comp
L Device:C C5
U 1 1 5C22B5D8
P 4200 2950
F 0 "C5" H 4315 2996 50  0000 L CNN
F 1 "22uF" H 4315 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4238 2800 50  0001 C CNN
F 3 "~" H 4200 2950 50  0001 C CNN
	1    4200 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5C22FA6F
P 2200 2950
F 0 "C4" H 2315 2996 50  0000 L CNN
F 1 "10uF" H 2315 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2238 2800 50  0001 C CNN
F 3 "~" H 2200 2950 50  0001 C CNN
	1    2200 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J0
U 1 1 5C264D60
P 10650 5250
F 0 "J0" H 10705 5717 50  0000 C CNN
F 1 "USB_B_Micro" H 10705 5626 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Amphenol_10103594-0001LF_Horizontal" H 10800 5200 50  0001 C CNN
F 3 "~" H 10800 5200 50  0001 C CNN
	1    10650 5250
	1    0    0    -1  
$EndComp
NoConn ~ 10950 5250
$Comp
L MyKiCadLibs-Lib:INDUCTOR L1
U 1 1 5C2833A2
P 3550 2800
F 0 "L1" V 3319 2800 40  0000 C CNN
F 1 "INDUCTOR" V 3395 2800 40  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric" V 3400 2700 60  0001 C CNN
F 3 "" H 3550 2800 60  0000 C CNN
F 4 "10uH" V 3471 2800 40  0000 C CNN "Current"
F 5 "Text description" V 3750 2850 40  0001 C CNN "Description"
F 6 "Size" V 3805 2800 40  0001 C CNN "Size"
	1    3550 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 2800 4050 2800
Connection ~ 4200 2800
Wire Wire Line
	4200 2800 4400 2800
Wire Wire Line
	4050 2900 4050 2800
Connection ~ 4050 2800
Wire Wire Line
	4050 2800 4200 2800
Wire Wire Line
	4200 3100 4200 3350
Connection ~ 2200 2800
Wire Wire Line
	2200 2800 2050 2800
Wire Wire Line
	2200 3100 2200 3350
Wire Wire Line
	2200 3350 2850 3350
$Comp
L Regulator_Linear:LD3985M47R_SOT23 U1
U 1 1 5C350A9E
P 2850 2900
F 0 "U1" H 2850 3242 50  0000 C CNN
F 1 "TOREX" H 2850 3151 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 2850 3225 50  0001 C CIN
F 3 "http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATASHEET/CD00003395.pdf" H 2850 2900 50  0001 C CNN
	1    2850 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2800 3150 2800
Wire Wire Line
	3150 2900 4050 2900
Wire Wire Line
	2850 3200 2850 3350
Connection ~ 2850 3350
Wire Wire Line
	2850 3350 4200 3350
Wire Wire Line
	10050 4000 10050 4050
Connection ~ 10050 4000
Wire Wire Line
	10050 1800 10050 2500
Wire Wire Line
	10050 2500 10050 3250
Connection ~ 10050 2500
Connection ~ 10050 3250
Connection ~ 8450 3250
Connection ~ 8450 4000
Wire Wire Line
	8450 4000 8450 4050
Connection ~ 9250 3250
Connection ~ 9250 4000
Wire Wire Line
	9250 4000 9250 4050
$Comp
L fifth-rescue:GND-power #PWR0109
U 1 1 5C306102
P 3350 7550
F 0 "#PWR0109" H 3350 7300 50  0001 C CNN
F 1 "GND" V 3355 7422 50  0000 R CNN
F 2 "" H 3350 7550 50  0001 C CNN
F 3 "" H 3350 7550 50  0001 C CNN
	1    3350 7550
	1    0    0    -1  
$EndComp
Connection ~ 3350 7550
$Comp
L fifth-rescue:GND-power #PWR0110
U 1 1 5C31BEA6
P 8350 6550
F 0 "#PWR0110" H 8350 6300 50  0001 C CNN
F 1 "GND" V 8355 6422 50  0000 R CNN
F 2 "" H 8350 6550 50  0001 C CNN
F 3 "" H 8350 6550 50  0001 C CNN
	1    8350 6550
	1    0    0    -1  
$EndComp
Connection ~ 8350 6550
$Comp
L fifth-rescue:GND-power #PWR0111
U 1 1 5C331B03
P 10650 5650
F 0 "#PWR0111" H 10650 5400 50  0001 C CNN
F 1 "GND" V 10655 5522 50  0000 R CNN
F 2 "" H 10650 5650 50  0001 C CNN
F 3 "" H 10650 5650 50  0001 C CNN
	1    10650 5650
	1    0    0    -1  
$EndComp
$Comp
L fifth-rescue:GND-power #PWR0113
U 1 1 5C352999
P 4200 3350
F 0 "#PWR0113" H 4200 3100 50  0001 C CNN
F 1 "GND" V 4205 3222 50  0000 R CNN
F 2 "" H 4200 3350 50  0001 C CNN
F 3 "" H 4200 3350 50  0001 C CNN
	1    4200 3350
	1    0    0    -1  
$EndComp
Connection ~ 4200 3350
$Comp
L fifth-rescue:GND-power #PWR0114
U 1 1 5C36829A
P 2200 3350
F 0 "#PWR0114" H 2200 3100 50  0001 C CNN
F 1 "GND" V 2205 3222 50  0000 R CNN
F 2 "" H 2200 3350 50  0001 C CNN
F 3 "" H 2200 3350 50  0001 C CNN
	1    2200 3350
	1    0    0    -1  
$EndComp
Connection ~ 2200 3350
$Comp
L fifth-rescue:GND-power #PWR0112
U 1 1 5C3840B5
P 1350 6800
F 0 "#PWR0112" H 1350 6550 50  0001 C CNN
F 1 "GND" V 1355 6672 50  0000 R CNN
F 2 "" H 1350 6800 50  0001 C CNN
F 3 "" H 1350 6800 50  0001 C CNN
	1    1350 6800
	0    1    1    0   
$EndComp
NoConn ~ 1650 5050
$EndSCHEMATC
