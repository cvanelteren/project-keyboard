from __future__ import print_function, unicode_literals
import pcbnew, dxfgrabber, numpy as np, re
fileName = 'fifth/fifth.kicad_pcb'
pcb = pcbnew.LoadBoard(fileName)
# width     = 11 # mm
origin    = (0, 0)


width   = 0
with open('centroids.txt', 'r') as f:
	centroids =  []
	for line in f.readlines():
		line = line.strip().split('\t')
		centroids.append( tuple( float(i) + width for i in line))

modules = {module.GetReference() : module for module in \
           pcb.GetModules() if 'K' in module.GetReference()}
func    = lambda x : int(re.search("\d+", x).group())

m = {module.GetReference(): module for module in pcb.GetModules()}
# py2 has no sorted dicts by default
#diode = 9.5
#print(m.keys())
#for module, center in zip(sorted(modules, key = func), centroids):
#    ob     = modules[module]
#    newPos = pcbnew.wxPoint(*(pcbnew.FromMM(i) for i in center[::-1]))
#    ob.SetPosition(newPos)
#    ob.SetOrientationDegrees(90)
#    print(module, ob.GetPosition(), center )
#    newPos = pcbnew.wxPoint(*(pcbnew.FromMM(i + diode if idx == 0 else i ) for idx, i in enumerate(center[::-1])))
#    m['D' + str(module.lower().split('k')[-1])].SetPosition(newPos)
#    m['D' + str(module.lower().split('k')[-1])].SetOrientationDegrees(90)
#pcb.Save(fileName)
