EESchema Schematic File Version 4
LIBS:corne-cherry-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Corne Cherry"
Date "2018-08-25"
Rev "2.0"
Comp "foostan"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW2
U 1 1 5BFAC492
P 5250 900
F 0 "SW2" H 5400 1010 50  0000 C CNN
F 1 "SW_PUSH" H 5250 820 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 5250 900 50  0001 C CNN
F 3 "" H 5250 900 50  0000 C CNN
	1    5250 900 
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D2
U 1 1 5BFAC493
P 5550 1050
F 0 "D2" H 5550 1150 50  0000 C CNN
F 1 "D" H 5550 950 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 5550 1050 50  0001 C CNN
F 3 "" H 5550 1050 50  0001 C CNN
	1    5550 1050
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW3
U 1 1 5BFAC494
P 5950 900
F 0 "SW3" H 6100 1010 50  0000 C CNN
F 1 "SW_PUSH" H 5950 820 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 5950 900 50  0001 C CNN
F 3 "" H 5950 900 50  0000 C CNN
	1    5950 900 
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D3
U 1 1 5BFAC495
P 6250 1050
F 0 "D3" H 6250 1150 50  0000 C CNN
F 1 "D" H 6250 950 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 6250 1050 50  0001 C CNN
F 3 "" H 6250 1050 50  0001 C CNN
	1    6250 1050
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW4
U 1 1 5A5E2908
P 6650 900
F 0 "SW4" H 6800 1010 50  0000 C CNN
F 1 "SW_PUSH" H 6650 820 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 6650 900 50  0001 C CNN
F 3 "" H 6650 900 50  0000 C CNN
	1    6650 900 
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW5
U 1 1 5A5E2933
P 7350 900
F 0 "SW5" H 7500 1010 50  0000 C CNN
F 1 "SW_PUSH" H 7350 820 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 7350 900 50  0001 C CNN
F 3 "" H 7350 900 50  0000 C CNN
	1    7350 900 
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW6
U 1 1 5A5E295E
P 8050 900
F 0 "SW6" H 8200 1010 50  0000 C CNN
F 1 "SW_PUSH" H 8050 820 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 8050 900 50  0001 C CNN
F 3 "" H 8050 900 50  0000 C CNN
	1    8050 900 
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D4
U 1 1 5A5E29BF
P 6950 1050
F 0 "D4" H 6950 1150 50  0000 C CNN
F 1 "D" H 6950 950 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 6950 1050 50  0001 C CNN
F 3 "" H 6950 1050 50  0001 C CNN
	1    6950 1050
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:D-Device D5
U 1 1 5BFAC49A
P 7650 1050
F 0 "D5" H 7650 1150 50  0000 C CNN
F 1 "D" H 7650 950 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 7650 1050 50  0001 C CNN
F 3 "" H 7650 1050 50  0001 C CNN
	1    7650 1050
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:D-Device D6
U 1 1 5BFAC49B
P 8350 1050
F 0 "D6" H 8350 1150 50  0000 C CNN
F 1 "D" H 8350 950 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 8350 1050 50  0001 C CNN
F 3 "" H 8350 1050 50  0001 C CNN
	1    8350 1050
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW1
U 1 1 5A5E2B19
P 4550 900
F 0 "SW1" H 4700 1010 50  0000 C CNN
F 1 "SW_PUSH" H 4550 820 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 4550 900 50  0001 C CNN
F 3 "" H 4550 900 50  0000 C CNN
	1    4550 900 
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D1
U 1 1 5BFAC49D
P 4850 1050
F 0 "D1" H 4850 1150 50  0000 C CNN
F 1 "D" H 4850 950 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 4850 1050 50  0001 C CNN
F 3 "" H 4850 1050 50  0001 C CNN
	1    4850 1050
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW8
U 1 1 5BFAC49E
P 5250 1500
F 0 "SW8" H 5400 1610 50  0000 C CNN
F 1 "SW_PUSH" H 5250 1420 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 5250 1500 50  0001 C CNN
F 3 "" H 5250 1500 50  0000 C CNN
	1    5250 1500
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D8
U 1 1 5BFAC49F
P 5550 1650
F 0 "D8" H 5550 1750 50  0000 C CNN
F 1 "D" H 5550 1550 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 5550 1650 50  0001 C CNN
F 3 "" H 5550 1650 50  0001 C CNN
	1    5550 1650
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW9
U 1 1 5BFAC4A0
P 5950 1500
F 0 "SW9" H 6100 1610 50  0000 C CNN
F 1 "SW_PUSH" H 5950 1420 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 5950 1500 50  0001 C CNN
F 3 "" H 5950 1500 50  0000 C CNN
	1    5950 1500
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D9
U 1 1 5BFAC4A1
P 6250 1650
F 0 "D9" H 6250 1750 50  0000 C CNN
F 1 "D" H 6250 1550 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 6250 1650 50  0001 C CNN
F 3 "" H 6250 1650 50  0001 C CNN
	1    6250 1650
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW10
U 1 1 5A5E2D3E
P 6650 1500
F 0 "SW10" H 6800 1610 50  0000 C CNN
F 1 "SW_PUSH" H 6650 1420 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 6650 1500 50  0001 C CNN
F 3 "" H 6650 1500 50  0000 C CNN
	1    6650 1500
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW11
U 1 1 5A5E2D44
P 7350 1500
F 0 "SW11" H 7500 1610 50  0000 C CNN
F 1 "SW_PUSH" H 7350 1420 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 7350 1500 50  0001 C CNN
F 3 "" H 7350 1500 50  0000 C CNN
	1    7350 1500
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW12
U 1 1 5A5E2D4A
P 8050 1500
F 0 "SW12" H 8200 1610 50  0000 C CNN
F 1 "SW_PUSH" H 8050 1420 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 8050 1500 50  0001 C CNN
F 3 "" H 8050 1500 50  0000 C CNN
	1    8050 1500
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D10
U 1 1 5A5E2D56
P 6950 1650
F 0 "D10" H 6950 1750 50  0000 C CNN
F 1 "D" H 6950 1550 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 6950 1650 50  0001 C CNN
F 3 "" H 6950 1650 50  0001 C CNN
	1    6950 1650
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:D-Device D11
U 1 1 5BFAC4A6
P 7650 1650
F 0 "D11" H 7650 1750 50  0000 C CNN
F 1 "D" H 7650 1550 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 7650 1650 50  0001 C CNN
F 3 "" H 7650 1650 50  0001 C CNN
	1    7650 1650
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:D-Device D12
U 1 1 5A5E2D62
P 8350 1650
F 0 "D12" H 8350 1750 50  0000 C CNN
F 1 "D" H 8350 1550 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 8350 1650 50  0001 C CNN
F 3 "" H 8350 1650 50  0001 C CNN
	1    8350 1650
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW7
U 1 1 5A5E2D6E
P 4550 1500
F 0 "SW7" H 4700 1610 50  0000 C CNN
F 1 "SW_PUSH" H 4550 1420 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 4550 1500 50  0001 C CNN
F 3 "" H 4550 1500 50  0000 C CNN
	1    4550 1500
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D7
U 1 1 5BFAC4A9
P 4850 1650
F 0 "D7" H 4850 1750 50  0000 C CNN
F 1 "D" H 4850 1550 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 4850 1650 50  0001 C CNN
F 3 "" H 4850 1650 50  0001 C CNN
	1    4850 1650
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW14
U 1 1 5BFAC4AA
P 5250 2100
F 0 "SW14" H 5400 2210 50  0000 C CNN
F 1 "SW_PUSH" H 5250 2020 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 5250 2100 50  0001 C CNN
F 3 "" H 5250 2100 50  0000 C CNN
	1    5250 2100
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D14
U 1 1 5BFAC4AB
P 5550 2250
F 0 "D14" H 5550 2350 50  0000 C CNN
F 1 "D" H 5550 2150 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 5550 2250 50  0001 C CNN
F 3 "" H 5550 2250 50  0001 C CNN
	1    5550 2250
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW15
U 1 1 5BFAC4AC
P 5950 2100
F 0 "SW15" H 6100 2210 50  0000 C CNN
F 1 "SW_PUSH" H 5950 2020 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 5950 2100 50  0001 C CNN
F 3 "" H 5950 2100 50  0000 C CNN
	1    5950 2100
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D15
U 1 1 5BFAC4AD
P 6250 2250
F 0 "D15" H 6250 2350 50  0000 C CNN
F 1 "D" H 6250 2150 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 6250 2250 50  0001 C CNN
F 3 "" H 6250 2250 50  0001 C CNN
	1    6250 2250
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW16
U 1 1 5BFAC4AE
P 6650 2100
F 0 "SW16" H 6800 2210 50  0000 C CNN
F 1 "SW_PUSH" H 6650 2020 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 6650 2100 50  0001 C CNN
F 3 "" H 6650 2100 50  0000 C CNN
	1    6650 2100
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW17
U 1 1 5BFAC4AF
P 7350 2100
F 0 "SW17" H 7500 2210 50  0000 C CNN
F 1 "SW_PUSH" H 7350 2020 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 7350 2100 50  0001 C CNN
F 3 "" H 7350 2100 50  0000 C CNN
	1    7350 2100
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW18
U 1 1 5BFAC4B0
P 8050 2100
F 0 "SW18" H 8200 2210 50  0000 C CNN
F 1 "SW_PUSH" H 8050 2020 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 8050 2100 50  0001 C CNN
F 3 "" H 8050 2100 50  0000 C CNN
	1    8050 2100
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D16
U 1 1 5BFAC4B1
P 6950 2250
F 0 "D16" H 6950 2350 50  0000 C CNN
F 1 "D" H 6950 2150 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 6950 2250 50  0001 C CNN
F 3 "" H 6950 2250 50  0001 C CNN
	1    6950 2250
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:D-Device D17
U 1 1 5BFAC4B2
P 7650 2250
F 0 "D17" H 7650 2350 50  0000 C CNN
F 1 "D" H 7650 2150 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 7650 2250 50  0001 C CNN
F 3 "" H 7650 2250 50  0001 C CNN
	1    7650 2250
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:D-Device D18
U 1 1 5A5E35ED
P 8350 2250
F 0 "D18" H 8350 2350 50  0000 C CNN
F 1 "D" H 8350 2150 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 8350 2250 50  0001 C CNN
F 3 "" H 8350 2250 50  0001 C CNN
	1    8350 2250
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW13
U 1 1 5BFAC4B4
P 4550 2100
F 0 "SW13" H 4700 2210 50  0000 C CNN
F 1 "SW_PUSH" H 4550 2020 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 4550 2100 50  0001 C CNN
F 3 "" H 4550 2100 50  0000 C CNN
	1    4550 2100
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D13
U 1 1 5A5E35FF
P 4850 2250
F 0 "D13" H 4850 2350 50  0000 C CNN
F 1 "D" H 4850 2150 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 4850 2250 50  0001 C CNN
F 3 "" H 4850 2250 50  0001 C CNN
	1    4850 2250
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW20
U 1 1 5A5E37A4
P 7350 2700
F 0 "SW20" H 7500 2810 50  0000 C CNN
F 1 "SW_PUSH" H 7350 2620 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 7350 2700 50  0001 C CNN
F 3 "" H 7350 2700 50  0000 C CNN
	1    7350 2700
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D20
U 1 1 5A5E37AA
P 7650 2850
F 0 "D20" H 7650 2950 50  0000 C CNN
F 1 "D" H 7650 2750 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 7650 2850 50  0001 C CNN
F 3 "" H 7650 2850 50  0001 C CNN
	1    7650 2850
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW21
U 1 1 5A5E37B0
P 8050 2700
F 0 "SW21" H 8200 2810 50  0000 C CNN
F 1 "SW_PUSH" H 8050 2620 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_150-dualsided" H 8050 2700 50  0001 C CNN
F 3 "" H 8050 2700 50  0000 C CNN
	1    8050 2700
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D21
U 1 1 5A5E37B6
P 8350 2850
F 0 "D21" H 8350 2950 50  0000 C CNN
F 1 "D" H 8350 2750 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 8350 2850 50  0001 C CNN
F 3 "" H 8350 2850 50  0001 C CNN
	1    8350 2850
	0    -1   -1   0   
$EndComp
$Comp
L corne-cherry-rescue:SW_PUSH-kbd SW19
U 1 1 5A5E37EC
P 6650 2700
F 0 "SW19" H 6800 2810 50  0000 C CNN
F 1 "SW_PUSH" H 6650 2620 50  0000 C CNN
F 2 "redox_w_footprints:Mx_Alps_100-dualside" H 6650 2700 50  0001 C CNN
F 3 "" H 6650 2700 50  0000 C CNN
	1    6650 2700
	1    0    0    -1  
$EndComp
$Comp
L corne-cherry-rescue:D-Device D19
U 1 1 5A5E37F2
P 6950 2850
F 0 "D19" H 6950 2950 50  0000 C CNN
F 1 "D" H 6950 2750 50  0000 C CNN
F 2 "redox_w_footprints:Diode-dual" H 6950 2850 50  0001 C CNN
F 3 "" H 6950 2850 50  0001 C CNN
	1    6950 2850
	0    -1   -1   0   
$EndComp
Text GLabel 4950 750  1    60   Input ~ 0
col1
Text GLabel 4250 750  1    60   Input ~ 0
col0
Text GLabel 5650 750  1    60   Input ~ 0
col2
Text GLabel 6350 750  1    60   Input ~ 0
col3
Text GLabel 7050 750  1    60   Input ~ 0
col4
Text GLabel 7750 750  1    60   Input ~ 0
col5
Text GLabel 4700 1200 0    60   Input ~ 0
row0
Text GLabel 4700 1800 0    60   Input ~ 0
row1
Text GLabel 4700 2400 0    60   Input ~ 0
row2
Text GLabel 6800 3000 0    60   Input ~ 0
row3
Connection ~ 6950 3000
Connection ~ 7650 3000
Wire Wire Line
	6800 3000 6950 3000
Wire Wire Line
	4700 2400 4850 2400
Wire Wire Line
	4700 1800 4850 1800
Wire Wire Line
	4700 1200 4850 1200
Wire Wire Line
	7750 750  7750 900 
Wire Wire Line
	7050 750  7050 900 
Wire Wire Line
	6350 750  6350 900 
Wire Wire Line
	5650 750  5650 900 
Wire Wire Line
	4950 750  4950 900 
Wire Wire Line
	4250 750  4250 900 
Connection ~ 7750 900 
Connection ~ 7050 900 
Connection ~ 5650 900 
Connection ~ 4950 900 
Connection ~ 4250 900 
Connection ~ 4250 1500
Connection ~ 7750 2100
Connection ~ 7750 1500
Connection ~ 7050 2100
Connection ~ 7050 1500
Connection ~ 6350 2100
Connection ~ 6350 900 
Connection ~ 6350 1500
Connection ~ 5650 1500
Connection ~ 4950 1500
Connection ~ 4850 2400
Connection ~ 5550 2400
Connection ~ 6250 2400
Connection ~ 6950 2400
Connection ~ 7650 2400
Connection ~ 4850 1800
Connection ~ 5550 1800
Connection ~ 6250 1800
Connection ~ 6950 1800
Connection ~ 7650 1800
Connection ~ 4850 1200
Connection ~ 5550 1200
Connection ~ 6250 1200
Connection ~ 6950 1200
Connection ~ 7650 1200
Wire Wire Line
	6950 3000 7650 3000
Wire Wire Line
	7650 3000 8350 3000
Wire Wire Line
	7750 900  7750 1500
Wire Wire Line
	7050 900  7050 1500
Wire Wire Line
	5650 900  5650 1500
Wire Wire Line
	4950 900  4950 1500
Wire Wire Line
	4250 900  4250 1500
Wire Wire Line
	4250 1500 4250 2100
Wire Wire Line
	7750 2100 7750 2700
Wire Wire Line
	7750 1500 7750 2100
Wire Wire Line
	7050 2100 7050 2700
Wire Wire Line
	7050 1500 7050 2100
Wire Wire Line
	6350 2100 6350 2700
Wire Wire Line
	6350 900  6350 1500
Wire Wire Line
	6350 1500 6350 2100
Wire Wire Line
	5650 1500 5650 2100
Wire Wire Line
	4950 1500 4950 2100
Wire Wire Line
	4850 2400 5550 2400
Wire Wire Line
	5550 2400 6250 2400
Wire Wire Line
	6250 2400 6950 2400
Wire Wire Line
	6950 2400 7650 2400
Wire Wire Line
	7650 2400 8350 2400
Wire Wire Line
	4850 1800 5550 1800
Wire Wire Line
	5550 1800 6250 1800
Wire Wire Line
	6250 1800 6950 1800
Wire Wire Line
	6950 1800 7650 1800
Wire Wire Line
	7650 1800 8350 1800
Wire Wire Line
	4850 1200 5550 1200
Wire Wire Line
	5550 1200 6250 1200
Wire Wire Line
	6250 1200 6950 1200
Wire Wire Line
	6950 1200 7650 1200
Wire Wire Line
	7650 1200 8350 1200
$Comp
L redox_rev1-cache:Device_Battery_Cell BT_LEFT1
U 1 1 5C0219D5
P 2300 6000
F 0 "BT_LEFT1" H 2418 6096 50  0000 L CNN
F 1 "Device_Battery_Cell" H 2418 6005 50  0000 L CNN
F 2 "redox_w_footprints:BatteryHolder_Keystone_3034_1x20mm" V 2300 6060 50  0001 C CNN
F 3 "" V 2300 6060 50  0001 C CNN
	1    2300 6000
	1    0    0    -1  
$EndComp
Text GLabel 2800 1350 2    50   Input ~ 0
SWCLK
Text GLabel 2800 1450 2    50   Input ~ 0
SWDIO
Text GLabel 900  1050 0    50   Input ~ 0
row3
Text GLabel 900  1150 0    50   Input ~ 0
row2
Text GLabel 900  1250 0    50   Input ~ 0
row1
Text GLabel 1300 2500 3    50   Input ~ 0
col0
Text GLabel 1400 2500 3    50   Input ~ 0
col1
Text GLabel 1800 2500 3    50   Input ~ 0
col4
Text GLabel 1600 2500 3    50   Input ~ 0
col2
Text GLabel 1700 2500 3    50   Input ~ 0
col3
Text GLabel 1900 2500 3    50   Input ~ 0
col5
$Comp
L redox_rev1-cache:power_VCC #PWR0113
U 1 1 5C114870
P 900 2050
F 0 "#PWR0113" H 900 1900 50  0001 C CNN
F 1 "power_VCC" V 918 2178 50  0000 L CNN
F 2 "" H 900 2050 50  0001 C CNN
F 3 "" H 900 2050 50  0001 C CNN
	1    900  2050
	0    -1   -1   0   
$EndComp
$Comp
L redox_rev1-cache:power_GND #PWR0114
U 1 1 5C15EA95
P 900 1950
F 0 "#PWR0114" H 900 1700 50  0001 C CNN
F 1 "power_GND" V 905 1823 50  0000 R CNN
F 2 "" H 900 1950 50  0001 C CNN
F 3 "" H 900 1950 50  0001 C CNN
	1    900  1950
	0    1    1    0   
$EndComp
$Comp
L redox_rev1-cache:core51822b_Core51822B U_RIGHT2
U 1 1 5C16E205
P 1950 3700
F 0 "U_RIGHT2" H 1950 4587 60  0000 C CNN
F 1 "core51822b_Core51822B" H 1950 4481 60  0000 C CNN
F 2 "redox_w_footprints:MY-YJ-14015-Module" H 1950 3800 60  0001 C CNN
F 3 "" H 1950 3800 60  0001 C CNN
	1    1950 3700
	1    0    0    -1  
$EndComp
Text GLabel 2900 3600 2    50   Input ~ 0
SWCLK
Text GLabel 2900 3700 2    50   Input ~ 0
SWDIO
Text GLabel 2900 3300 2    50   Input ~ 0
row3
Text GLabel 2900 3400 2    50   Input ~ 0
row2
Text GLabel 2900 3500 2    50   Input ~ 0
row1
Text GLabel 1900 4750 3    50   Input ~ 0
col5
Text GLabel 2000 4750 3    50   Input ~ 0
col4
Text GLabel 2100 4750 3    50   Input ~ 0
col3
Text GLabel 2200 4750 3    50   Input ~ 0
col2
Text GLabel 2400 4750 3    50   Input ~ 0
col1
Text GLabel 2500 4750 3    50   Input ~ 0
col0
$Comp
L redox_rev1-cache:power_VCC #PWR0115
U 1 1 5C16E216
P 1000 4300
F 0 "#PWR0115" H 1000 4150 50  0001 C CNN
F 1 "power_VCC" V 1018 4428 50  0000 L CNN
F 2 "" H 1000 4300 50  0001 C CNN
F 3 "" H 1000 4300 50  0001 C CNN
	1    1000 4300
	0    -1   -1   0   
$EndComp
$Comp
L redox_rev1-cache:power_GND #PWR0116
U 1 1 5C16E21C
P 1000 4200
F 0 "#PWR0116" H 1000 3950 50  0001 C CNN
F 1 "power_GND" V 1005 4073 50  0000 R CNN
F 2 "" H 1000 4200 50  0001 C CNN
F 3 "" H 1000 4200 50  0001 C CNN
	1    1000 4200
	0    1    1    0   
$EndComp
$Comp
L redox_rev1-cache:Connector_Conn_01x04_Male J_PROG_PORT1
U 1 1 5C2EFD07
P 2350 7100
F 0 "J_PROG_PORT1" H 2456 7378 50  0000 C CNN
F 1 "Conn_01_X04_MALE" H 2456 7287 50  0000 C CNN
F 2 "redox_w_footprints:PinHeader_1x04_P2.54mm_Horizontal" H 2350 7100 50  0001 C CNN
F 3 "" H 2350 7100 50  0001 C CNN
	1    2350 7100
	1    0    0    -1  
$EndComp
$Comp
L redox_rev1-cache:power_VCC #PWR0117
U 1 1 5C30F1B8
P 2550 7000
F 0 "#PWR0117" H 2550 6850 50  0001 C CNN
F 1 "power_VCC" V 2567 7128 50  0000 L CNN
F 2 "" H 2550 7000 50  0001 C CNN
F 3 "" H 2550 7000 50  0001 C CNN
	1    2550 7000
	0    1    1    0   
$EndComp
Text GLabel 2550 7100 2    50   Input ~ 0
SWCLK
Text GLabel 2550 7200 2    50   Input ~ 0
SWDIO
$Comp
L redox_rev1-cache:power_GND #PWR0118
U 1 1 5C31E74A
P 2550 7300
F 0 "#PWR0118" H 2550 7050 50  0001 C CNN
F 1 "power_GND" V 2555 7172 50  0000 R CNN
F 2 "" H 2550 7300 50  0001 C CNN
F 3 "" H 2550 7300 50  0001 C CNN
	1    2550 7300
	0    -1   -1   0   
$EndComp
$Comp
L redox_rev1-cache:Device_Battery_Cell BT_RIGHT1
U 1 1 5C377512
P 3600 6000
F 0 "BT_RIGHT1" H 3718 6096 50  0000 L CNN
F 1 "Device_Battery_Cell" H 3718 6005 50  0000 L CNN
F 2 "redox_w_footprints:BatteryHolder_Keystone_3034_1x20mm" V 3600 6060 50  0001 C CNN
F 3 "" V 3600 6060 50  0001 C CNN
	1    3600 6000
	1    0    0    -1  
$EndComp
$Comp
L redox_rev1-cache:Switch_SW_SPDT SW_RIGHT1
U 1 1 5C3B3007
P 3600 5600
F 0 "SW_RIGHT1" V 3646 5412 50  0000 R CNN
F 1 "Switch_SW_SPDT" V 3555 5412 50  0000 R CNN
F 2 "Buttons_Switches_THT:SW_DIP_x1_W7.62mm_Slide_LowProfile" H 3600 5600 50  0001 C CNN
F 3 "" H 3600 5600 50  0001 C CNN
	1    3600 5600
	0    -1   -1   0   
$EndComp
$Comp
L redox_rev1-cache:Switch_SW_SPDT SW_LEFT1
U 1 1 5C3B3133
P 2300 5600
F 0 "SW_LEFT1" V 2346 5412 50  0000 R CNN
F 1 "Switch_SW_SPDT" V 2255 5412 50  0000 R CNN
F 2 "Buttons_Switches_THT:SW_DIP_x1_W7.62mm_Slide_LowProfile" H 2300 5600 50  0001 C CNN
F 3 "" H 2300 5600 50  0001 C CNN
	1    2300 5600
	0    -1   -1   0   
$EndComp
$Comp
L redox_rev1-cache:power_GND #PWR0119
U 1 1 5C3B3AB3
P 2300 6100
F 0 "#PWR0119" H 2300 5850 50  0001 C CNN
F 1 "power_GND" H 2305 5927 50  0000 C CNN
F 2 "" H 2300 6100 50  0001 C CNN
F 3 "" H 2300 6100 50  0001 C CNN
	1    2300 6100
	1    0    0    -1  
$EndComp
$Comp
L redox_rev1-cache:power_GND #PWR0120
U 1 1 5C3B3BBC
P 3600 6100
F 0 "#PWR0120" H 3600 5850 50  0001 C CNN
F 1 "power_GND" H 3605 5927 50  0000 C CNN
F 2 "" H 3600 6100 50  0001 C CNN
F 3 "" H 3600 6100 50  0001 C CNN
	1    3600 6100
	1    0    0    -1  
$EndComp
Text GLabel 900  1850 0    50   Input ~ 0
row0
NoConn ~ 2900 4300
NoConn ~ 2900 3900
NoConn ~ 2900 3800
NoConn ~ 2900 3200
NoConn ~ 1000 3300
NoConn ~ 1000 3400
NoConn ~ 1000 3500
NoConn ~ 1000 3600
NoConn ~ 1000 3700
NoConn ~ 1000 3800
NoConn ~ 1000 3900
NoConn ~ 1000 4000
NoConn ~ 1000 4100
NoConn ~ 1700 4750
NoConn ~ 1800 4750
NoConn ~ 2300 4750
NoConn ~ 2000 2500
NoConn ~ 2100 2500
NoConn ~ 2800 2050
NoConn ~ 2800 1950
NoConn ~ 2800 1850
NoConn ~ 2800 1750
NoConn ~ 2800 1650
NoConn ~ 2800 1550
NoConn ~ 2800 1250
NoConn ~ 2800 1150
NoConn ~ 2800 1050
NoConn ~ 2800 950 
$Comp
L redox_rev1-cache:power_GND #PWR0121
U 1 1 5C6200F9
P 900 950
F 0 "#PWR0121" H 900 700 50  0001 C CNN
F 1 "power_GND" V 905 823 50  0000 R CNN
F 2 "" H 900 950 50  0001 C CNN
F 3 "" H 900 950 50  0001 C CNN
	1    900  950 
	0    1    1    0   
$EndComp
$Comp
L redox_rev1-cache:power_GND #PWR0122
U 1 1 5C62041D
P 1000 3200
F 0 "#PWR0122" H 1000 2950 50  0001 C CNN
F 1 "power_GND" V 1005 3073 50  0000 R CNN
F 2 "" H 1000 3200 50  0001 C CNN
F 3 "" H 1000 3200 50  0001 C CNN
	1    1000 3200
	0    1    1    0   
$EndComp
NoConn ~ 1500 2500
NoConn ~ -5050 4900
$Comp
L redox_rev1-cache:power_VCC #PWR0123
U 1 1 5C1C673C
P 3500 5400
F 0 "#PWR0123" H 3500 5250 50  0001 C CNN
F 1 "power_VCC" H 3517 5573 50  0000 C CNN
F 2 "" H 3500 5400 50  0001 C CNN
F 3 "" H 3500 5400 50  0001 C CNN
	1    3500 5400
	1    0    0    -1  
$EndComp
$Comp
L redox_rev1-cache:power_VCC #PWR0124
U 1 1 5C1D5D19
P 2200 5400
F 0 "#PWR0124" H 2200 5250 50  0001 C CNN
F 1 "power_VCC" H 2217 5573 50  0000 C CNN
F 2 "" H 2200 5400 50  0001 C CNN
F 3 "" H 2200 5400 50  0001 C CNN
	1    2200 5400
	1    0    0    -1  
$EndComp
NoConn ~ 2400 5400
NoConn ~ 3700 5400
$Comp
L redox_rev1-cache:core51822b_Core51822B U_RIGHT1
U 1 1 5C022796
P 1850 1450
F 0 "U_RIGHT1" H 1850 2337 60  0000 C CNN
F 1 "core51822b_Core51822B" H 1850 2231 60  0000 C CNN
F 2 "redox_w_footprints:MY-YJ-14015-Module" H 1850 1550 60  0001 C CNN
F 3 "" H 1850 1550 60  0001 C CNN
	1    1850 1450
	1    0    0    -1  
$EndComp
Text GLabel 2900 4100 2    50   Input ~ 0
row0
$EndSCHEMATC
