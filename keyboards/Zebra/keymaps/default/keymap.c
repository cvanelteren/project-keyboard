/* Copyright 2017 REPLACE_WITH_YOUR_NAME
 *
 * This program is free software             : you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include "Zebra.h"
// added by me // Layer shorthand /*

#define _QW 0
#define _FN 1
#define _FN2 2

//Tap Dance Declarations
enum {
  TD_ESC_CAPS = 0,
  TD_WORD_LEFT,
  TD_WORD_RIGHT,
  TAP_BRACKET_LEFT,
  TAP_BRACKET_RIGHT,
  TAP_BSLSH_EQ,
  TAP_QUOTE,
  TAP_SCLN
};

//Tap Dance Definitions
qk_tap_dance_action_t tap_dance_actions[] = {
  //Tap once for Esc, twice for Caps Lock
  [TD_ESC_CAPS] = ACTION_TAP_DANCE_DOUBLE(KC_ESC, KC_CAPS),
  [TD_WORD_LEFT] = ACTION_TAP_DANCE_DOUBLE(KC_LEFT, LCTL(KC_LEFT)),
  [TD_WORD_RIGHT] = ACTION_TAP_DANCE_DOUBLE(KC_RIGHT, LCTL(KC_RIGHT)),
  [TAP_BRACKET_LEFT] = ACTION_TAP_DANCE_DOUBLE(KC_LBRC, LSFT(KC_LBRC)),
  [TAP_BRACKET_RIGHT] = ACTION_TAP_DANCE_DOUBLE(KC_RBRC, LSFT(KC_RBRC)),
  [TAP_BSLSH_EQ] = ACTION_TAP_DANCE_DOUBLE( KC_EQL, KC_BSLS),
  [TAP_QUOTE] = ACTION_TAP_DANCE_DOUBLE( KC_QUOT, LSFT(KC_QUOT)),
  [TAP_SCLN] = ACTION_TAP_DANCE_DOUBLE(KC_SCLN, LSFT(KC_SCLN))
// Other declarations would go here, separated by commas, if you have them
};

enum custom_keycodes{
  NEW_LINE = SAFE_RANGE,
};

// bool process_record_user(uint16_t keycode, keyrecord_t *record) {
//   if (record -> event.pressed)
//   {
//     if (get_mods() && MOD_BIT(KC_LCTL) && MOD_BIT(KC_ENT) )
//     {
//       SEND_STRING("TEST" );
//       return false;
//     }
//     else
//       return true;
//     }
//   return true;
// };


//In Layer declaration, add tap dance item in place of a key code
// TD(TD_ESC_CAPS)aaaa

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

 [_QW] = { 
     { KC_GESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_MINS, KC_GRV, KC_EQL, KC_6, KC_7, KC_8, KC_9, KC_0, KC_BSPC},
 { KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_LBRC, KC_BSLS, KC_RBRC, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_BSPC},
 { KC_GESC , KC_A, KC_S, KC_D, KC_F, KC_G, KC_HOME, KC_DEL, KC_PGUP, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT },
 { KC_LSPO, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_END, KC_UP, KC_PGDN, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSPC },
 { KC_LCTL, KC_LGUI,  MO(_FN), KC_LALT, KC_LCTL, LT(_FN2 , KC_SPC), KC_LEFT, KC_DOWN, KC_RGHT, LT(_FN, KC_ENT), KC_LCTL, MO(_FN), KC_RALT, KC_RGUI, KC_RCTL },
},

 [_FN] = { 
  { KC_GESC, KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, KC_TRNS, RESET},
  {KC_CAPS, _______, KC_BSLS, KC_LCBR, KC_RCBR, KC_PLUS, _______, _______, _______, _______, _______, KC_HOME, KC_END, _______, _______},
  { KC_GRV, KC_UNDS, KC_EQL, KC_LBRC, KC_RBRC, KC_EQL, _______, _______, _______,KC_LEFT, KC_DOWN, KC_UP, KC_RIGHT,  _______, _______},
  { KC_TRNS, KC_VOLD, KC_MUTE, KC_VOLU, _______, KC_MINS, _______, _______, _______, _______, _______, _______, _______, _______, _______},
  { KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______},
},
 [_FN2] = {
   {KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, KC_TRNS, KC_TRNS, _______},
   {_______, KC_1 , KC_2, KC_3, KC_4,_______, _______,_______, _______, _______, _______ , _______, _______ , _______, _______},
   {TO(_QW), KC_5, KC_6, KC_7, KC_8,_______, _______,_______, _______, KC_LEFT, KC_DOWN, KC_UP, KC_RIGHT ,_______, _______},
   {_______, KC_7, KC_8, KC_9, KC_0, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______},
   {_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, RESET},
 }



};

const uint16_t PROGMEM fn_actions[] = {

};

const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
{
  // MACRODOWN only works in this function
      switch(id) {
        case 0                               :
          if (record->event.pressed) {
            register_code(KC_RSFT);
            #ifdef BACKLIGHT_ENABLE
              backlight_step();
            #endif
          } else {
            unregister_code(KC_RSFT);
          }


        break;
      }
    return MACRO_NONE;
};
